package com.kun.lazzy.foodgood;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

public class MainActivity extends AppCompatActivity {

    GridLayout gridLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridLayout = (GridLayout)findViewById((R.id.mainGrid));
        setSingleEvent(gridLayout);
    }

    private void setSingleEvent(GridLayout gridLayout){
        for(int i = 0; i<gridLayout.getChildCount(); i ++){
            CardView cardView = (CardView)gridLayout.getChildAt(i);
            final  int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(MainActivity.this, "Clicked at index "+ finalI, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
